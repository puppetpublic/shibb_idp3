class shibb_idp3(
  $env                = 'uat',
  $pool_slaves        = [],
  $pool_master        = undef,
  $ldap_host          = '',
  $mysql_support      = true,
)
{

  if (!$env) {
    fail("missing env parameter (prod, uat, etc.)")
  }

  if (!$pool_master) {
    fail("missing datasealer pool master")
  }

  if (size($pool_slaves) == 0) {
    fail("missing datasealer pool slave array")
  }

  # $pool_fqdn will be the name to refer to the IDP pool and depends on
  # the environment:
  #   prod:  idp.stanford.edu
  #   uat:   idp-uat.stanford.edu
  #   test:  idp-test.stanford.edu
  #   dev:   idp-dev.stanford.edu
  #   sbx:   idp-sbx.stanford.edu
  #
  # $pool_hostname is just the hostname (e.g., "idp-uat").
  case $env {
    'prod': {
      $pool_fqdn     = 'idp.stanford.edu'
      $pool_hostname = 'idp'
      $host_suffix   = ''
    }
    default: {
      $pool_fqdn     = "idp-${env}.stanford.edu"
      $pool_hostname = "idp-${env}"
      $host_suffix   = "-${env}"
    }
  }

  include shibb_idp3::tomcat8

  include shibb_idp3::install

  include shibb_idp3::apache2

  include shibb_idp3::config

  include shibb_idp3::datasealer

  include shibb_idp3::ldap

  include shibb_idp3::lb

  include shibb_idp3::cron

  include shibb_idp3::remctl

}
