# The IdP uses public-key encryption to sign responses and allow
# encryption of authentication requests. These certificate portion should
# be self-signed certificate of at least 2048-bits with a distant
# expiration (at least 10 years).

# NOTE! This key-pair is NOT the same as the key-pair used by the Apache
# server.

# EXAMPLE 1.
#
#   shibb_idp3::metadata_key {
#     metadata_key_name => 'idp-metadata-2',
#     wallet_key_name   => 'ssl-key/idp-uat.stanford.edu/idp-metadata-2',
#     certfile_source   => 'puppet:///modules/cert-files/idp-uat-metadata-2.pem',
#     active_key        => false,
#   }
#
# This will put a copy of the wallet object
#   ssl-key/idp-uat.stanford.edu/idp-metadata1
# in
#   /etc/ssl/private/idp-metadata1.key
# and a copy of the certificate pointed to by the certfile_source in
#   /etc/ssl/certs/idp-metadata1.pem


# EXAMPLE 2.
#
#   shibb_idp3::metadata_key {
#     metadata_key_name => 'idp-metadata-primary',
#     wallet_key_name   => 'ssl-key/idp-prod.stanford.edu/idp-metadata-primary',
#     certfile_source   => 'puppet:///modules/cert-files/idp-prod-metadata-primary.pem',
#     active_key        => true,
#   }
#
#
# This will put a copy of the wallet object
#
#   ssl-key/idp-prod.stanford.edu/idp-metadata-primary
#
# in
#
#   /etc/ssl/private/idp-metadata-primary.key
#
# and a copy of the certificate pointed to by the certfile_source in
#
#   /etc/ssl/certs/idp-metadata-primary.pem
#
# Furthermore, it will also create softlinks to the above two files in
# /opt/shibboleth/credentials.


define shibb_idp3::metadata_key (
  $metadata_key_name = $name,
  $wallet_key_name   = '',
  $certfile_source  = '',
  $active_key_flag   = false,
) {

  # Install the private-key.
  base::wallet { $wallet_key_name:
    type   => 'file',
    path   => "/etc/ssl/private/${metadata_key_name}.key",
    owner  => 'tomcat8',
    group  => 'root',
    mode   => '0640';
  }

  # Install the certificate.
  file { "/etc/ssl/certs/${metadata_key_name}.pem":
    source => $certfile_source,
    mode   => '0644',
  }

  # If this is the active key, make the link.
  if ($active_key_flag) {
    file { '/opt/shibboleth-idp/credentials/idp-saml.key':
      ensure  => link,
      target  => "/etc/ssl/private/${metadata_key_name}.key",
      require => Package['shibboleth-identity-provider'],
    }

    file { '/opt/shibboleth-idp/credentials/idp-saml.pem':
      ensure  => link,
      target  => "/etc/ssl/certs/${metadata_key_name}.pem",
      require => Package['shibboleth-identity-provider'],
    }
  }

}
