# If the datasealer file is not present, run datasealer_update.pl so it
# gets created.

class shibb_idp3::datasealer
{

  # The datasealer_update.pl script is in the stanford-shibbolethidp3-tool
  # package.
  package { 'stanford-shibbolethidp3-tools':
    ensure  => installed,
    require => Package['shibboleth-identity-provider'],
  }

  # We need the datasealer master/slave configuration file.
  file { '/etc/shibboleth-idp/dist-datasealer.conf':
    content => template('shibb_idp3/etc/shibboleth-idp/dist-datasealer.conf.erb'),
    require => Package['stanford-shibbolethidp3-tools'],
  }

  include shibb_idp3::root_k5login

  # Run this command only if the sealer file does NOT exist. This will
  # create the datasealer JKS file.
  exec { 'create_datasealer_file':
    command => 'datasealer-admin update',
    path    => ['/usr/bin', '/usr/sbin'],
    creates => '/opt/shibboleth-idp/credentials/sealer.jks',
    require => [
                 Package['stanford-shibbolethidp3-tools'],
                 Package['shibboleth-identity-provider'],
                 File['/etc/shibboleth-idp/dist-datasealer.conf'],
                 File['/etc/shibboleth-idp/conf/idp.properties'],
               ],
  }

}
