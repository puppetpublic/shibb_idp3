# Set up IdP-related remctl commands
class shibb_idp3::remctl {

 file { '/etc/remctl/conf.d/idp':
   source => 'puppet:///modules/shibb_idp3/etc/remctl/conf.d/idp';
 }

}
