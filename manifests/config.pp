#
# $access_control_ips: an array of IP addresses that should have
# access to administrative pages (e.g., the status page). These addresses
# should be valid CIDR ranges. Note that these are IN ADDITION to the ones
# we allow by default (localhost, load-balancer, other IdP servers, Adam
# and Vivien's workstation).
# Example:
#
# shibb_idp3::config::access_control_allowedRanges:
#   - 171.67.218.160/28
#   - 172.27.218.160/28
#   - 171.64.19.46/32
#
# Note that '127.0.0.1/32' and '::1/128' are automatically added to this
# list.
# DEFAULT: [] (the empty array)
#
# $access_checkInterval: sets the property
# idp.service.access.checkInterval in the services.xml file. This is time
# is how often access-control.xml should be checked for reload.
# DEFAULT: 'PT5M'
#
# $relyingparty_checkInterval: sets the property
# idp.service.relyingparty.checkInterval in the services.xml file. This is time
# is how often relying-party.xml should be checked for reload.
# DEFAULT: 'PT2M'
#
# $access_nameidGeneration: sets the property
# idp.service.nameidGeneration.checkInterval in the services.xml
# file. This time is how often saml-nameid.xml should be checked for
# reload.  DEFAULT: 'PT2M'
#
# $nameid_checkInterval: sets the property
# idp.service.nameidGeneration.checkInterval in the services.xml
# file. This time is how often saml-nameid.xml should be checked for
# reload.  DEFAULT: 'PT2M'
#
# $filter_checkInterval: sets the property
# idp.service.attribute.filter.checkInterval in the services.xml
# file. This time is how often attribute-filter.xml should be checked for
# reload.  DEFAULT: 'PT2M'
#
# $resolver_checkInterval: sets the property
# idp.service.attribute.resolver.checkInterval in the services.xml
# file. This time is how often attribute-resolver.xml should be checked for
# reload.  DEFAULT: 'PT2M'
#
# $logging_checkInterval: sets the property
# idp.service.logging.checkInterval in the services.xml
# file. This time is how often logback.xml should be checked for
# reload.  DEFAULT: 'PT2M'
#
# $loghistory: the number of old log files to keep on LOCAL disk.
# DEFAULT: 15


class shibb_idp3::config (
  $relyingparty_checkInterval   = 'PT2M',
  $access_checkInterval         = 'PT5M',
  $nameid_checkInterval         = 'PT2M',
  $filter_checkInterval         = 'PT2M',
  $resolver_checkInterval       = 'PT2M',
  $logging_checkInterval        = 'PT2M',
  $access_control_allowedRanges = [],
  $loghistory                   = '15',
){

  # Let's make some local copies of variables that will be useful in the
  # ERB files.
  $pool_fqdn = "${shibb_idp3::pool_fqdn}"

  ## tomcat8 configuration
  file { '/etc/default/tomcat8':
    ensure  => present,
    source  => 'puppet:///modules/shibb_idp3/etc/default/tomcat8',
    mode    => '0644',
  }

  # Default file resource attributes
  File {
    ensure => 'present',
    mode   => '0644',
    owner  => 'tomcat8',
    group  => 'tomcat8',
  }


  ### PROPERTIES FILES

  # idp.properties
  include shibb_idp3::config::idp_properties

  # services.properties
  # Controls the dynamic reloading behavior of
  # services listed services.xml. Right now, we want relying-party
  # resources to use legacy V2 format.
  file { '/etc/shibboleth-idp/conf/services.properties':
    content => template('shibb_idp3/etc/shibboleth-idp/conf/services.properties.erb'),
    require => Package['shibboleth-identity-provider'],
  }

  ### Create the directory where the SP metadata files go.
  file { '/etc/shibboleth-idp/metadata':
    ensure  => directory,
    require => Package['shibboleth-identity-provider'],
  }


  ### The tomcat web.xml file

  # 1. web.xml: custom version contains TwoStepRemoteUser definition.
  file { '/opt/shibboleth-idp/webapp/WEB-INF/web.xml':
    content => template('shibb_idp3/opt/shibboleth-idp/webapp/WEB-INF/web.xml.erb'),
    require => Package['shibboleth-identity-provider'],
  }


  ### LOGGING

  # logback.xml: log-level settings. Note that the parameter
  # loghistory is used in logback.xml.erb and sets the number of old log
  # files we keep on local disk.
  #
  # Being Debian, we put the logs in /var/log/shibboleth-idp.
  file { '/var/log/shibboleth-idp':
    ensure => directory,
  }
  file { '/opt/shibboleth-idp/logs':
    ensure => link,
    target => '/var/log/shibboleth-idp',
  }
  file { '/etc/shibboleth-idp/conf/logback.xml':
    content => template('shibb_idp3/etc/shibboleth-idp/conf/logback.xml.erb'),
    require => Package['shibboleth-identity-provider'],
  }

  # We want to run some of the shibboleth-idp log files through
  # filter-syslog and sent to root mail so we can see if there are any
  # system problems.

  # The idp-warn log file contains messages of WARN or higher severity. We
  # want to see these messages in our root mail. We use a daily cron job
  # for this. Note that for this to work it is CRUCIAL that the
  # filter-syslog job run
  file { '/etc/filter-syslog/idp-warn':
    source  => 'puppet:///modules/shibb_idp3/etc/filter-syslog/idp-warn',
  }

  # The format of the audit log is set by conf/audit.xml. We add the
  # nameid type field to the default set of attributes included.
  file { '/etc/shibboleth-idp/conf/audit.xml':
    content => template('shibb_idp3/etc/shibboleth-idp/conf/audit.xml.erb'),
    require => Package['shibboleth-identity-provider'],
  }


  # 2. access-control.xml: allow access to status page
  file { '/etc/shibboleth-idp/conf/access-control.xml':
    content => template('shibb_idp3/etc/shibboleth-idp/conf/access-control.xml.erb'),
    require => Package['shibboleth-identity-provider'],
  }


  # 3. saml-nameid.xml
  include shibb_idp3::config::saml_nameid



  #### FLOWS

  # general-authn.xml: some customizations
  file { '/etc/shibboleth-idp/conf/authn/general-authn.xml':
    content => template('shibb_idp3/etc/shibboleth-idp/conf/authn/general-authn.xml.erb'),
    require => Package['shibboleth-identity-provider'],
  }


  # Install the custom flow files
  shibb_idp3::config::flow { 'TwoStepRemoteUser': }
  shibb_idp3::config::flow { 'RemoteUserForceReAuth': }
  shibb_idp3::config::flow { 'TwoStepRemoteUserForceReAuth': }


}
