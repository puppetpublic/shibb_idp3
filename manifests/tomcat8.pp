class shibb_idp3::tomcat8 {
  package { 'tomcat8':
    ensure => present,
  }

  # Install the tomcat8 main configuration file.
  file { '/etc/tomcat8/server.xml':
    source  => 'puppet:///modules/shibb_idp3/etc/tomcat8/server.xml',
    require => Package['tomcat8'],
  }

  user { 'tomcat8':
    groups  => ['www-data', 'ssl-cert'],
    require => Package['tomcat8']
  }

}
