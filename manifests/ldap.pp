# 1. Download the kerberos principal required for accessing the OpenLDAP
#    directory.

class shibb_idp3::ldap (
  $ldap_keytab_path = '/opt/shibboleth-idp/credentials/shibboleth.keytab',
  $ldap_baseDN      = 'cn=people,dc=stanford,dc=edu',
  $ldap_principal_name_override = '',
) {

  # We want the ldapsearch utility
  package { 'ldap-utils':
    ensure => installed
  }

  if ($ldap_principal_name_override == "") {
    case "${shibb_idp3::env}" {
      'prod':  {
        $ldap_principal_name = 'service/shibboleth'
      }
      default: {
        $ldap_principal_name = "service/shibboleth-${shibb_idp3::env}"
      }
    }
  } else {
    $ldap_principal_name = $ldap_principal_name_override
  }

  # Get the keytab wallet object.
  base::wallet { $ldap_principal_name:
    ensure => present,
    path   => $ldap_keytab_path,
    owner  => 'root',
    group  => 'tomcat8',
    mode   => '0640',
    require => Package['shibboleth-identity-provider'],
  }

  # Install the krb5_jaas.config file. This file contains the Kerberos
  # principal name and keytab path. It expects that the two variables
  # $ldap_principal_name and $ldap_keytab_path are set.
  #
  # There should be no reason to change $ldap_keytab_path, but
  # $ldap_principal_name depends on the environment (dev, uat, prod, etc.).
  file { '/etc/shibboleth-idp/conf/authn/krb5_jaas.config':
    ensure  => present,
    content => template('shibb_idp3/etc/shibboleth-idp/conf/authn/krb5_jaas.config.erb'),
    owner   => 'tomcat8',
    group   => 'tomcat8',
    mode    => '0644',
    require => Package['shibboleth-identity-provider'],
  }

  # Configure the ldap.properties file.

  # If shibb_idp3::ldap_host is defined, use that. Otherwise, use the
  # following lookup table. Because the non-production LDAP environments
  # can have incorrect user data, we point everything at production LDAP.
  if ("${shibb_idp3::ldap_host}" == "") {
    case "${shibb_idp3::env}" {
      'prod':  { $ldap_host = 'ldap.stanford.edu' }
      default: { $ldap_host = 'ldap.stanford.edu' }
    }
  } else {
    $ldap_host = "${shibb_idp3::ldap_host}"
  }


  # We need to merge the ldap.properties file with a MySQL database
  # credentials file.
  $ldap_properties_template_file = '/etc/shibboleth-idp/conf/ldap.properties.template'
  $ldap_properties_file          = '/etc/shibboleth-idp/conf/ldap.properties'
  $mysql_credentials_file        = '/etc/shibboleth-idp/conf/mysql_credentials'

  # (1)
  file { $ldap_properties_template_file:
    content => template('shibb_idp3/etc/shibboleth-idp/conf/ldap.properties.template.erb'),
    require => Package['shibboleth-identity-provider'],
  }

  # (2)
  $mysql_credenitals_name = "db/its-idg/${shibb_idp3::pool_hostname}/s_shibboleth"

  base::wallet { $mysql_credenitals_name:
    type   => 'file',
    path   => $mysql_credentials_file,
    owner  => 'root',
    group  => 'root',
    mode   => '0640';
  }

  # (3)
  exec { "generate-conf $ldap_properties_file":
    command     => "generate-conf --template $ldap_properties_template_file --config $mysql_credentials_file --newfile $ldap_properties_file",
    refreshonly => true,
    subscribe   => [
                    File[$ldap_properties_template_file],
                    Base::Wallet[$mysql_credenitals_name],
                    ]
  }

  # Set the file permissions??
  exec { "set_permissions $ldap_properties_file":
    command     => "chown tomcat8:tomcat8 $ldap_properties_file; chmod 0640 $ldap_properties_file",
    refreshonly => true,
    subscribe   => [
      Exec["generate-conf $ldap_properties_file"],
      ],
  }


}
