class shibb_idp3::lb {
  # Install the bigip pool management helper
  lb::bigip { 'lb':
    ensure      => present,
    signal_port => 9080,
  }
}

