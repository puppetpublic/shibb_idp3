# Install and configure Apache.

# $archive_in_afs: If set to true (the default), when apache logs are
# rotated they will be copied into AFS.
#
# $use_comodo_legacy_bundle: Use the legacy InCommon certificate chain
# file rather than /etc/ssl/certs in order to support older systems. See
# templates/etc/apache2/sites-available/shibboleth-idp.erb for more
# information. Note that setting this to true after March 2020 will BREAK
# clients.
# DEFAULT: false

class shibb_idp3::apache2(
  $archive_in_afs = true,
  $use_comodo_legacy_bundle = false,
){

  ## We use Stanford's apache module (also installs webauth).
  include apache

  ## Enable some Apache modules

  # We need libapache2-mod-jk to connect Apache to the tomcat.
  package {
    'libapache2-mod-jk':         ensure => present;
    'libapache2-mod-proxy-html': ensure => present;
    'libapache2-mod-auth-kerb':  ensure => present;
  }

  # Enable the Apache Java reverse proxy module (AJP13)
  apache::module { 'proxy_ajp':
    ensure  => present,
  }

  apache::module { 'jk':
    ensure  => present,
    require => Package['libapache2-mod-jk'],
  }

  ## Set up the Apache site file.

  $apache_pool_fqdn     = $shibb_idp3::pool_fqdn
  $apache_pool_hostname = $shibb_idp3::pool_hostname

  # Set up the Apache site that acts as the front-end for Shibboleth. We
  # protect the Shibboleth site with WebAuth.
  apache::site { 'shibboleth-idp':
    ensure  => present,
    content => template('shibb_idp3/etc/apache2/sites-available/shibboleth-idp.erb'),
  }

  # We manage the Apache mod_jk worker.properties file.
  file { '/etc/libapache2-mod-jk/workers.properties':
    content => template('shibb_idp3/etc/libapache2-mod-jk/workers.properties.erb'),
    require => Package['libapache2-mod-jk'],
  }

  # We have an index file at the root so that requests for
  # http://server/ go to Stanford's Shibboleth service page.
  # to https.
  file { '/var/www/index.html':
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/shibb_idp3/var/www/index.html',
  }

  # Remove default apache html directory and its index.html
  file { 'delete /var/www/html':
    ensure => absent,
    path => '/var/www/html',
    recurse => true,
    force => true,
    purge => true
  }

  file { 'publish idp cert' :
    ensure  => present,
    path    => '/var/www/idp.crt',
    links   => follow,
    source  => 'file:///opt/shibboleth-idp/credentials/idp-saml.pem',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }


  # We want the "officially" signed certificate and private key for the
  # Apache front end. By default, apache::cert::comodo puts the key pair
  # in /etc/ssl/certs/server.pem and /etc/ssl/private/server.key which is
  # exactly where we want them to be.
  apache::cert::comodo { "${apache_pool_hostname}":
    ensure  => present,
    keyname => "ssl-key/${apache_pool_fqdn}",
  }


  # Enable the Apache Kerberos module
  apache::module { 'auth_kerb':
    ensure  => present,
    require => Package['libapache2-mod-auth-kerb'],
  }

  # Keytab used for ECP authentication on production systems
  case "${shibb_idp3::env}" {
    'prod': {
      base::wallet { 'HTTP/idp-lb.stanford.edu':
        path   => '/etc/http-krb5.keytab',
        owner  => 'root',
        group  => 'www-data',
        mode   => '0640',
        ensure => present;
      }
    }
    'uat': {
      base::wallet { 'HTTP/idp-uat-lb.stanford.edu':
        path   => '/etc/http-krb5.keytab',
        owner  => 'root',
        group  => 'www-data',
        mode   => '0640',
        ensure => present;
      }
    }
  }

  ## Apache Logs

  # Rotate Apache logs
    base::newsyslog::config { 'apache':
      frequency => 'daily',
      directory => '/var/log/apache2',
      restart   => 'run /usr/sbin/apache2ctl graceful',
      logs      => [
        'error.log',
        'other_vhosts_access.log',
        'shibboleth-idp-sso.access_log',
        'shibboleth-idp-sso.error_log',
        'mod_jk.log',
      ],
    }

  # If we want to archive the logs in AFS, set that now.
  if ($archive_in_afs) {
    Base::Newsyslog::Config['apache'] {
      archive => '/afs/ir/service/auth/logs/idp',
    }
  }




}
