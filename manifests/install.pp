# 1. Install the shibboleth-identity-provider tomcat application package and
# its web.xml.
#
# 2. Set up the directory for the configuration.

# To do basic test of installation:
#
#   JAVA_HOME=/usr/lib/jvm/default-java /opt/shibboleth-idp/bin/status.sh
class shibb_idp3::install {

  package { 'shibboleth-identity-provider':
    ensure => present,
  }

  # Add the XML file to /etc/tomcat8/Catalina/localhost that points to the root
  # of the Shibboleth IdP application. This is how tomcat knows where the
  # shibboleth-idp application is installed.
  file { '/etc/tomcat8/Catalina/localhost/idp.xml':
    source => 'puppet:///modules/shibb_idp3/etc/tomcat8/Catalina/localhost/idp.xml',
  }

  # If we are using MySQL, install the JDBC MySQL support
  if ($shibb_idp3::mysql_support) {
    package { 'libmysql-java': ensure => present }

    # We need to link the mysql java connector jar into the tomcat8
    # directory so that tomcat8 will be able to find it.
    file { '/usr/share/tomcat8/lib/mysql-connector-java.jar':
      ensure  => 'link',
      target  => '/usr/share/java/mysql-connector-java.jar',
      require => Package['libmysql-java'],
    }
  }
}
