class shibb_idp3::cron {

  # Make a daily cron job to update and replicate the DataSealer files.
  file { '/etc/cron.daily/datasealer':
    source  => 'puppet:///modules/shibb_idp3/etc/cron.daily/datasealer',
  }

}
