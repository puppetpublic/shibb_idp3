# If you want to allow other kerberos principals to login as root add them
# using the $extra_rootlogins parameter. For example, if using hiera:
#
#  # Allow Scotty to login as root.
#  shibb_idp3::root_k5login::extra_rootlogins:
#    - swl/root@stanford.edu

class shibb_idp3::root_k5login (
  $extra_rootlogins = [],
) inherits user::root {
  # We want to copy the datasealer file from the master to the slaves. To
  # do whit we use scp and the master host's keytab. Thus, we need to put
  # the master host's keytab in /root/.k5login. We do the simple thing and
  # stick in enough servers to cover expansion of the pool later.

  case "${shibb_idp3::env}" {
    'prod': {
      $host_principals = [
        "host/idp${shibb_idp3::host_suffix}01.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}02.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}03.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}04.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}05.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}06.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}07.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}08.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}09.stanford.edu@stanford.edu",
      ]
    }
    default: {
      $host_principals = [
        "host/idp${shibb_idp3::host_suffix}1.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}2.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}3.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}4.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}5.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}6.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}7.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}8.stanford.edu@stanford.edu",
        "host/idp${shibb_idp3::host_suffix}9.stanford.edu@stanford.edu",
      ]
    }
  }

  $all_principals = concat($host_principals, $extra_rootlogins)

  K5login['/root/.k5login'] { principals +> $all_principals }

}

#  K5login['/root/.k5login'] {
#        principals +> [
#          "host/idp${shibb_idp3::host_suffix}1.stanford.edu@stanford.edu",
#          "host/idp${shibb_idp3::host_suffix}2.stanford.edu@stanford.edu",
#          "host/idp${shibb_idp3::host_suffix}3.stanford.edu@stanford.edu",
#          "host/idp${shibb_idp3::host_suffix}4.stanford.edu@stanford.edu",
#          "host/idp${shibb_idp3::host_suffix}5.stanford.edu@stanford.edu",
#        ],
#      }
#    }
