class shibb_idp3::config::idp_properties {

  ## idp.properties
  # Add idp.properties configuration file. This file has a password in it
  # so we (1) install a template file first, then (2) a small wallet file with the
  # password in it, then (3) merge the two to get the final idp.properies file.

  $datasealer_template_file = '/etc/shibboleth-idp/conf/idp.properties.template'
  $idp_properties_file      = '/etc/shibboleth-idp/conf/idp.properties'
  $datasealer_password_file = '/opt/shibboleth-idp/credentials/datasealer.password'

  # (1)
  file { $datasealer_template_file:
    ensure  => present,
    content => template('shibb_idp3/etc/shibboleth-idp/conf/idp.properties.template.erb'),
    owner   => 'tomcat8',
    group   => 'tomcat8',
    mode    => '0644',
  }

  # (2)
  case "${shibb_idp3::env}" {
    'prod':  {
      $datasealer_name = 'config/its-idg/idp/datasealer'
    }
    default: {
      $datasealer_name = "config/its-idg/idp-${shibb_idp3::env}/datasealer"
    }
  }

  base::wallet { $datasealer_name:
    type   => 'file',
    path   => $datasealer_password_file,
    owner  => 'root',
    group  => 'root',
    mode   => '0640';
  }

  # (3)
  exec { "generate-conf $idp_properties_file":
    command     => "generate-conf --template $datasealer_template_file --config $datasealer_password_file --newfile $idp_properties_file",
    refreshonly => true,
    subscribe   => [
                    File[$datasealer_template_file],
                    Base::Wallet[$datasealer_name],
                    ]
  }

  # Set file permissions on $idp_properties_file file
  file { $idp_properties_file:
    owner   => 'root',
    group   => 'tomcat8',
    mode    => '0640',
    require => Exec["generate-conf $idp_properties_file"],
  }

}
