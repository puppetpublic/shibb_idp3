# Configure saml-nameid.properties file and saml-nameid.xml.

class shibb_idp3::config::saml_nameid {

  file { '/etc/shibboleth-idp/conf/saml-nameid.xml':
    source  => 'puppet:///modules/shibb_idp3/etc/shibboleth-idp/conf/saml-nameid.xml',
  }

  # Add saml-nameid.properties configuration file. This file has a salt in it,
  # so we (1) install a template file first, then (2) a small wallet file with the
  # salt in it, then (3) merge the two to get the final
  # saml-nameid.properties file.
  #
  # We do all this because we don't want to leave the salt (a potentially
  # sensitive security paramter) in a possibly public git repository.

  $persistentid_salt_template_file = '/etc/shibboleth-idp/conf/saml-nameid.properties.template'
  $saml_nameid_properties_file     = '/etc/shibboleth-idp/conf/saml-nameid.properties'
  $persistentid_salt_file          = '/etc/shibboleth-idp/conf/persistentid.salt'

  # (1)
  file { $persistentid_salt_template_file:
    content => template('shibb_idp3/etc/shibboleth-idp/conf/saml-nameid.properties.template.erb'),
    require => Package['shibboleth-identity-provider'],
  }

  # (2)
  case "${shibb_idp3::env}" {
    'prod':  {
      $persistentid_salt_name = 'config/its-idg/idp/persistentid_salt'
    }
    default: {
      $persistentid_salt_name = "config/its-idg/idp-${shibb_idp3::env}/persistentid_salt"
    }
  }

  base::wallet { $persistentid_salt_name:
    type   => 'file',
    path   => $persistentid_salt_file,
    owner  => 'root',
    group  => 'root',
    mode   => '0640';
  }

  # (3)
  exec { "generate-conf $saml_nameid_properties_file":
    command     => "generate-conf --template $persistentid_salt_template_file --config $persistentid_salt_file --newfile $saml_nameid_properties_file",
    refreshonly => true,
    subscribe   => [
                    File[$persistentid_salt_template_file],
                    Base::Wallet[$persistentid_salt_name],
                    ]
  }

  # Set file permissions on $saml_nameid_properties_file
  file { $saml_nameid_properties_file:
    owner   => 'root',
    group   => 'tomcat8',
    mode    => '0640',
    require => Exec["generate-conf $saml_nameid_properties_file"],
  }


}
