#
# Installs:
#  /opt/shibboleth-idp/flows/authn/FLOWNAME/flowname-authn-beans.xml
#  /opt/shibboleth-idp/flows/authn/FLOWNAME/flowname-authn-flow.xml
#  /etc/shibboleth-idp/conf/authn/flowname-authn-config.xml
#
# Edit these files manually:
#
# * conf/authn/general-authn.xml
# * webapp/WEB-INF/web.xml

define shibb_idp3::config::flow (
  $flow_base_dir = '/opt/shibboleth-idp/flows/authn',
) {

  $flow_name_lc = downcase($name)
  $flow_dir     = "${flow_base_dir}/${name}"

  $beans_file = "${flow_dir}/${flow_name_lc}-authn-beans.xml"
  $flow_file  = "${flow_dir}/${flow_name_lc}-authn-flow.xml"
  $conf_file  = "/etc/shibboleth-idp/conf/authn/${flow_name_lc}-authn-config.xml"

  # Create the flow directory
  file { $flow_dir:
    ensure => directory,
    require => Package['shibboleth-identity-provider'];
  }

  file { $beans_file:
    source  => "puppet:///modules/shibb_idp3/${beans_file}",
    require => File[$flow_dir],
  }

  file { $flow_file:
    source  => "puppet:///modules/shibb_idp3/${flow_file}",
    require => File[$flow_dir],
  }

  file { $conf_file:
    source  => "puppet:///modules/shibb_idp3/${conf_file}",
    require => Package['shibboleth-identity-provider'],
  }

}
